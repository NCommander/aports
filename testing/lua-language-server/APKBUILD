# Maintainer: psykose <alice@ayaya.dev>
pkgname=lua-language-server
pkgver=3.5.2
pkgrel=0
pkgdesc="Language Server for Lua"
url="https://github.com/sumneko/lua-language-server"
arch="all !s390x !ppc64le" # ftbfs
license="MIT"
makedepends="bash samurai"
source="https://github.com/sumneko/lua-language-server/archive/refs/tags/$pkgver/lua-language-server-$pkgver.tar.gz
	lua-language-server-submodules-$pkgver.zip.noauto::https://github.com/sumneko/lua-language-server/releases/download/$pkgver/lua-language-server-$pkgver-submodules.zip
	wrapper
	"
options="!check" # no tests

prepare() {
	default_prepare

	unzip -o "$srcdir"/lua-language-server-submodules-$pkgver.zip.noauto \
		-d "$builddir"
}

build() {
	ninja -C 3rd/luamake -f compile/ninja/linux.ninja
	./3rd/luamake/luamake rebuild
}

package() {
	install -Dm755 "$srcdir"/wrapper "$pkgdir"/usr/bin/lua-language-server
	install -Dm755 bin/lua-language-server \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 bin/main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 debugger.lua main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server
	cp -a locale meta script "$pkgdir"/usr/lib/lua-language-server
}

sha512sums="
bde798a60d52979d4332f434b7b57c2e1bde0393d6d14cb17ef504261400c44a561f70f7e17bd100828ec11fbe2360d4f1e34c9e570161e99de1f86757ceda8e  lua-language-server-3.5.2.tar.gz
e8ac3cfcf567d44f96254392dbad548a8983c20be282e74c540c1d444a1cdf6dcb77675e70e02f3a76cab6a3fda7d136df7d2f482e3b1049399e34a66f0f6fad  lua-language-server-submodules-3.5.2.zip.noauto
9fa9621b61a365a576079731afe419245268b5223292989d2f98091a26b8866ba97a8c6c4cf8e5cbb2704089cb45167630557049430105a71ed4fd55311a543a  wrapper
"
